clear
echo "-------------------------------------------------"
echo "Build program..."
g++ *.h *.cpp -o u04ex_exe
echo "-------------------------------------------------"
echo "TEST: Run program 1"
echo ""
echo "EXPECTED OUTPUT:"
echo "1. Display original variables num1, num2, num3, their values and addresses."
echo "2. Set ptr to nullptr, display address it's pointing to."
echo "3. Set ptr to num1's address, display address and value (via dereference)."
echo "    Ask user to enter new value."
echo "4. Set ptr to num2's address, display address and value (via dereference)."
echo "    Ask user to enter new value."
echo "5. Set ptr to num3's address, display address and value (via dereference)."
echo "    Ask user to enter new value."
echo "6. Display updated variables num1, num2, num3, their values and addresses."
echo ""
echo "ACTUAL OUTPUT:"
echo 1 50 75 90 | ./u04ex_exe
echo ""
echo "-------------------------------------------------"
echo "PROGRAM CODE:"
cat program1.cpp


echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "TEST: Run program 2"
echo ""
echo "EXPECTED OUTPUT:"
echo "1. Display list of products (index, name, price)"
echo "2. Ask user to enter index"
echo "3a. If index is valid: ptr updated; user asked to enter new price and new name."
echo "3b. If index is invalid: ptr isn't updated; nullptr not dereferenced."
echo "4. Display updated list of products"
echo ""
echo "ACTUAL OUTPUT (valid input):"
echo 2 0 2.99 Test | ./u04ex_exe
echo ""
echo "ACTUAL OUTPUT (invalid input):"
echo 2 5 | ./u04ex_exe
echo ""
echo "-------------------------------------------------"
echo "PROGRAM CODE:"
cat program2.cpp

echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "TEST: Run program 3"
echo ""
echo "EXPECTED OUTPUT:"
echo "1. Create nodes and link them."
echo "2. Iterate through nodes."
echo "3. Free memory."
echo ""
echo "ACTUAL OUTPUT:"
echo 3 | ./u04ex_exe
echo ""
echo "-------------------------------------------------"
echo "PROGRAM CODE:"
cat program3.cpp

echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "TEST: Run program 4"
echo ""
echo "EXPECTED OUTPUT:"
echo "1. Array of size 2 created."
echo "2. Ask user to enter items."
echo "3. Once item 3 is entered, array resizes."
echo "4. When QUIT typed, old memory freed."
echo ""
echo "ACTUAL OUTPUT:"
echo 4 cat dog mouse deer QUIT | ./u04ex_exe
echo ""
echo "-------------------------------------------------"
echo "PROGRAM CODE:"
cat program4.cpp

echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "TEST: Run program 5 (similar to program 4)"
echo ""
echo "EXPECTED OUTPUT:"
echo "1. Array of size 2 created."
echo "2. Ask user to enter items."
echo "3. Once item 3 is entered, array resizes."
echo "4. When QUIT typed program ends."
echo ""
echo "ACTUAL OUTPUT:"
echo 5 cat dog mouse deer QUIT | ./u04ex_exe
echo ""
echo "-------------------------------------------------"
echo "PROGRAM CODE:"
cat program5.cpp

echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "TEST: Run program 6 (similar to program 3)"
echo ""
echo "EXPECTED OUTPUT:"
echo "1. Create SmartNodes and link them."
echo "2. Iterate through nodes."
echo ""
echo "ACTUAL OUTPUT:"
echo 6 | ./u04ex_exe
echo ""
echo "-------------------------------------------------"
echo "PROGRAM CODE:"
cat program6.cpp

echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "TEST: Run program 7"
echo ""
echo "EXPECTED OUTPUT:"
echo "1. Series of Customer and Employees are created in 2 vectors."
echo "2. Table of Persons displayed."
echo ""
echo "ACTUAL OUTPUT:"
echo 7 | ./u04ex_exe
echo ""
echo "-------------------------------------------------"
echo "PROGRAM CODE:"
echo "- program7.cpp -"
cat program7.cpp
echo ""
echo "- p7_Person.h -"
cat p7_Person.h
echo ""
echo "- p7_Person.cpp -"
cat p7_Person.cpp
echo ""






