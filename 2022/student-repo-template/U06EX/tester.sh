clear
echo "-------------------------------------------------"
echo "Build program..."
g++ *.cpp program1/*.cpp program1/*.h program2/*.cpp program2/*.h program3/*.cpp program3/*.h -o u06ex_exe
echo "-------------------------------------------------"
echo "TEST: Run program 1"
echo ""
echo "EXPECTED OUTPUT:"
echo "Nothing displayed to the screen, but six text/html files created."
echo ""
echo "ACTUAL OUTPUT:"
echo 1 | ./u06ex_exe
echo ""
echo "-------------------------------------------------"
echo "PROGRAM CODE:"
echo "- p1_functions.cpp -"
cat program1/p1_functions.cpp
echo "- program1.cpp -"
cat program1/program1.cpp

echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "TEST: Run program 2"
echo ""
echo "EXPECTED OUTPUT:"
echo "Two tables shown, an original table and a copied table."
echo "Both storing the same values, but each element should have a different address."
echo ""
echo "ACTUAL OUTPUT:"
echo 2 | ./u06ex_exe
echo ""
echo "-------------------------------------------------"
echo "PROGRAM CODE:"
echo "- p2_class.cpp -"
cat program2/p2_class.cpp
echo "- program2.cpp -"
cat program2/program2.cpp

echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "TEST: Run program 3"
echo ""
echo "EXPECTED OUTPUT:"
echo "Program creates a few MyString objects. Asks user to enter a 3 letter word."
echo "Gets each letter of the word, compares word to secretWord, creates fullWord."
echo ""
echo "ACTUAL OUTPUT:"
echo 3 c a t | ./u06ex_exe
echo ""
echo "ACTUAL OUTPUT:"
echo 3 b i g | ./u06ex_exe
echo ""
echo "-------------------------------------------------"
echo "PROGRAM CODE:"
echo "- p3_MyString.cpp -"
cat program3/p3_MyString.cpp
echo "- program3.cpp -"
cat program3/program3.cpp

