#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

int main( int argumentCount, char *argumentList[] )
{
    if ( argumentCount < 2 )
    {
        cout << "EXPECTED FORMAT:" << endl;
        cout << argumentList[0] << " filename" << endl;
        cout << endl;
        return 1;
    }

    string filepath = argumentList[1];

    cout << "Loading image \"" << filepath << "\"..." << endl;

    ifstream input( filepath );
    if ( input.fail() )
    {
        cout << "Error! Cannot find the file!" << endl;
        return 2;
    }

    int width = 0;
    int height = 0;
    int r = 0, g = 0, b = 0;
    int x = 0;
    int y = 0;
    string buffer;

    vector<sf::RectangleShape> pixels;

    // Read in all the pixels
    getline( input, buffer ); // P3
    getline( input, buffer ); // Comment
    input >> width >> height;
    input >> buffer; // Color depth

    while ( input >> r >> g >> b )
    {
        sf::RectangleShape pixel;
        pixel.setPosition( x, y );
        pixel.setSize( sf::Vector2f( 1, 1 ) );
        pixel.setFillColor( sf::Color( r, g, b ) );
        pixel.setOutlineColor( sf::Color::Transparent );
        pixels.push_back( pixel );

        x++;
        if ( x >= width )
        {
            y++;
            x = 0;
        }
    }

    cout << pixels.size() << " pixels loaded" << endl;

    string titlebar = filepath + " (Rachel's PPM Viewer)";

    sf::RenderWindow window( sf::VideoMode( width, height ), titlebar );

    while ( window.isOpen() )
    {
        sf::Event event;
        while ( window.pollEvent( event ) )
        {
            if ( event.type == sf::Event::Closed )
            {
                window.close();
            }
        }

        window.clear( sf::Color::White );

        for ( auto& pixel : pixels )
        {
            window.draw( pixel );
        }

        window.display();
    }


    return 0;
}
