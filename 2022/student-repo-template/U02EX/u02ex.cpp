#include <iostream>
using namespace std;

void Program1();
void Program2();
void Program3();
void Program4();
void Program5();

int main()
{
//    cout << "PROGRAM NAME" << endl;
//
//    bool done = false;
//    while ( !done )
//    {
//        cout << "0. Exit" << endl;
//        cout << "1. Option A" << endl;
//        cout << "2. Option B" << endl;
//        cout << "3. Option C" << endl;
//
//        cout << endl << ">> ";
//        int choice;
//        cin >> choice;
//
//        if ( choice == 0 )  // Exit
//        {
//            done = true;
//        }
//
//        // Add additional checks
//    }
//
//    cout << endl << "PROGRAM END" << endl;
//    return 0;


    cout << "Run which program? (1-5): ";
    int prog;
    cin >> prog;

    switch( prog )
    {
        case 1: Program1(); break;
        case 2: Program2(); break;
        case 3: Program3(); break;
        case 4: Program4(); break;
        case 5: Program5(); break;
    }

    return 0;
}
