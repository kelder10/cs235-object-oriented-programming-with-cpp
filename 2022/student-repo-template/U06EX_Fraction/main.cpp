#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

#include "Fraction.h"
#include "FractionTester.h"

string ToStr( bool val )
{
    return ( val ) ? "true" : "false";
}

int main()
{
    cout << "1. Run unit tests" << endl;
    cout << "2. Run program" << endl;

    int choice;
    cout << endl << "Choice: ";
    cin >> choice;

    if ( choice == 1 )
    {
        FractionTester tester;
        tester.RunTests();
        return 0;
    }

    cout << endl << "FRACTION PROGRAM" << endl;


    return 0;
}

